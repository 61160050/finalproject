import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/homepage.dart';
import 'package:example_project/todo_form.dart';
import 'package:example_project/todo_infomation.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final Future<FirebaseApp> _initalization = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
     return FutureBuilder(
        future: _initalization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text("Error..");
          } else if (snapshot.connectionState == ConnectionState.done) {
            return MyApp();
          }
          return Text("Loading..");
        });

  }
}

class MyApp extends StatelessWidget {
  static final ValueNotifier<ThemeMode> themeNotifier =
      ValueNotifier(ThemeMode.light);
      
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ThemeMode>(
        valueListenable: themeNotifier,
        builder: (_, ThemeMode currentMode, __) {
          return MaterialApp(
            // Remove the debug banner
            debugShowCheckedModeBanner: false,
            title: 'Todo-List',
            theme: ThemeData(primarySwatch: Colors.blue),
            darkTheme: ThemeData.dark(),
            themeMode: currentMode,
            home: HomePage(),
          );
        });
  }
}
