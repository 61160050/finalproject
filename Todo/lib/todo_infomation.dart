import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:example_project/todo_form.dart';
import 'package:flutter/material.dart';

class TodoInfomation extends StatefulWidget {
  TodoInfomation({Key? key}) : super(key: key);

  @override
  _TodoInfomationState createState() => _TodoInfomationState();
}

class _TodoInfomationState extends State<TodoInfomation> {
  final Stream<QuerySnapshot> _TodoStream = FirebaseFirestore.instance
      .collection('Todo')
      .where('Status',isEqualTo: false)
      .snapshots();
  CollectionReference Todo = FirebaseFirestore.instance.collection('Todo');
  
  bool status = true;

  Future<void> delTodo(todoId) {
    return Todo
        .doc(todoId)
        .delete()
        .then((value) => print('Todo Delete'))
        .catchError((error) => print("Failed to Delete Todo $error"));
  }
  
  Future<void> statusupdate(todoId){
     return Todo
    .doc(todoId)
    .update({
      'Status': this.status
    })
      .then((value) => print("Todo Updated"))
      .catchError((error) => print("Failed to Updated Todo: $error"));
  }
  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _TodoStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went Wrong!!");
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading..");
        }
        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            return ListTile(
              title: Text(data['Title']),
                      subtitle: Text(data['Description']),
                      trailing:Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget> [
                  IconButton(
                icon: Icon(Icons.check_outlined),
                onPressed: () async {
                  await statusupdate(document.id);
                },
              ),
                  IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    await delTodo(document.id);
                },
              ),
              ],
              )  
            );
          }).toList(),
        );
      },
    );
  }
}
