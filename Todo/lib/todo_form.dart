
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class TodoForm extends StatefulWidget {
  TodoForm({Key? key, required this.todoId}) : super(key: key);

  final String todoId;

  @override
  _TodoFormState createState() => _TodoFormState(this.todoId);
}

class _TodoFormState extends State<TodoForm> {
  String todoId;
  String title = "";
  String descirption = "";
  bool status = false;
  _TodoFormState(this.todoId);
  TextEditingController _titleControler = new TextEditingController();
  TextEditingController _descirptionControler = new TextEditingController();
  
  CollectionReference Todo = FirebaseFirestore.instance.collection('Todo');

  Future<void> addTodo(){
    return Todo
    .add({
      'Title': this.title,
      'Description': this.descirption,
      'Status': this.status
    })
      .then((value) => print("Todo Added"))
      .catchError((error) => print("Failed to add Todo: $error"));
  }

  Future<void> UpdateTodo(){
    return Todo
    .doc(this.todoId)
    .update({
      'Title': this.title,
      'Description': this.descirption,
    })
      .then((value) => print("Todo Updated"))
      .catchError((error) => print("Failed to Updated Todo: $error"));
  }


   @override
  void initState() {
    super.initState();
    if (this.todoId.isNotEmpty) {
      Todo.doc(this.todoId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          title = data["Title"];
          descirption = data["Description"];
          status = data["Status"];
          _titleControler.text = title;
          _descirptionControler.text = descirption;
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เพิ่มสิ่งที่ต้องทำ'),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: _titleControler,
              decoration: InputDecoration(labelText: 'หัวข้อ'),
              onChanged: (value) {
                setState(() {
                  title = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "กรุณาใส่หัวข้อ";
                }
                return null;
              },
            ),
            TextFormField(
              controller: _descirptionControler,
              decoration: InputDecoration(labelText: 'รายละเอียด'),
              onChanged: (value) {
                setState(() {
                  descirption = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "กรุณาใส่รายละเอียด";
                }
                return null;
              },
            ),
            Padding(padding: EdgeInsets.all(8)),
            ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    if (todoId.isNotEmpty) {
                      await UpdateTodo();
                    } else {
                      await addTodo();
                    }

                    Navigator.pop(context);
                  }
                },
                child: Text("Save"))
          ],
        ),
      ),
    );
  }
}

