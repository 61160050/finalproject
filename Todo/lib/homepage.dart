import 'package:example_project/todo_form.dart';
import 'package:example_project/todo_infomation.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) =>Scaffold(
      appBar: AppBar(
        title: const Text('Todo-List'),
        actions: [
          IconButton(
              icon: Icon(MyApp.themeNotifier.value == ThemeMode.light
                  ? Icons.lightbulb
                  : Icons.lightbulb),
              onPressed: () {
                MyApp.themeNotifier.value =
                    MyApp.themeNotifier.value == ThemeMode.light
                        ? ThemeMode.dark
                        : ThemeMode.light;
              })
        ],
      ),
      body: TodoInfomation(),
      floatingActionButton: FloatingActionButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        backgroundColor: Colors.amber[800],
        onPressed: ()  {
         Navigator.push(context, MaterialPageRoute(builder: (context) => TodoForm(todoId :"")));
        },
        tooltip: 'เพิ่มสิ่งที่ต้องทำ',
        child: Icon(Icons.add),
      ),
    ); 
}
